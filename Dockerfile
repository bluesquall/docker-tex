FROM debian:bullseye-slim
MAINTAINER https://bitbucket.org/bluesquall/docker-tex/issues

RUN apt-get update && apt-get install -y wget perl && apt-get clean
COPY minimal-no-docs.profile /tmp/tl/
RUN wget -O- http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz \
  | tar -xvz --strip-components=1 --directory /tmp/tl \
  && /tmp/tl/install-tl --profile=/tmp/tl/minimal-no-docs.profile \
  && rm -rf /tmp/tl
